var site = {
	init: function(){
		site.popup();
		site.frameTransition();
	},

	slider: function(){
		var elem = document.getElementById('slideBanner');
		window.mySwipe = Swipe(elem, {
			auto: 4000,
			continuous: true,
			speed: 700
		});
		$('.prev a').click(function(){
			mySwipe.prev();
			return false;
		});
		$('.next a').click(function(){
			mySwipe.next();
			return false;
		});
	},

	imgZoom: function(){
		$('.jqzoom').jqzoom({
			zoomType: 'standard',
			lens:true,
			preloadImages: false,
			alwaysOn:false,
			zoomWidth: 350,
			zoomHeight: 525
		});
	},

	tabContent: function(){
		$(".tab_content").hide();
		$(".tabs li:first").addClass("selected").show();
		$(".tab_content:first").show();

		$(".tabs li").click(function() {
			$(".tabs li").removeClass("selected");
			$(this).addClass("selected");
			$(".tab_content").hide();
			var activeTab = $(this).find("a").attr("href");
			$(activeTab).fadeIn();
			return false;
		});
	},

	memberOrders: function(){
		$(".member .orders li table").hide();

		$('.member .orders li').toggleClass('orderlist');
		$('.orderlist > a').click(function(){
			if($(this).next('table').is(':visible')){
				$(this).next('table').hide();
				$(this).parent().removeClass('open');
			}
			else{
				$('.member .orders li table').hide();
				$('.orderlist').removeClass('open');
				$(this).parent().addClass('open');
				$(this).next("table").toggle();
			}
			return false;
		});
		$(".member .orders li:first-child").addClass("open");

	},

	frameTransition: function(){
		var windowH = $(window).height();
		$(window).resize(function(){
			windowH = $(window).height();
		});
		$(document).mousemove(function(e){
			var scrollT = $(document).scrollTop();
			var posY = e.pageY - scrollT;
			var shadowH = 200;
			var leftY = posY - shadowH;
			var rightY = windowH - leftY - shadowH;
			if (leftY < 0){
				leftY = 0;
			}
			if(rightY > windowH - shadowH){
				rightY = windowH - shadowH;
			}
			$(".border .left").css('backgroundPosition','0px '+leftY+'px');
			$(".border .right").css('backgroundPosition','0px '+rightY+'px');
		});
	},

	popup: function(){
		$(".topMenu a").click(function(){
			var rel = $(this).attr("rel");
			$(".popup").fadeOut();
			$("#"+rel).fadeIn();
		});
		$(".fgp").click(function(){
			$(".popup").fadeOut();
			$("#fpsPopup").fadeIn();
		});
		$(".popup a.close").click(function(){
			$(".popup").fadeOut();
		});
	}
};